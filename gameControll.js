var model = {
    access: true,
    guessed: 0,
    // Размер игрового поля (значение) * 2
    boardSize: 10,
    
    constructor: function (numShips, shipLenght, type, enemy){
        this.type = type;
        this.enemy = enemy;
        this.hits = 0;
        this.hited = false;
        this.misses = 0;
        this.shipsSunk = 0;
        this.numShips = numShips;
        this.shipLength = shipLenght;
        this.ships = [];
        this.locations = [""];
        
        this.ships.push({locations:[],hits:["","","",""],collision:[],length:4,number:1});
        this.ships.push({locations:[],hits:["","",""],collision:[],length:3,number:2});
        this.ships.push({locations:[],hits:["","",""],collision:[],length:3,number: 3});
        this.ships.push({locations:[],hits:["",""],collision:[],length:2,number:4});
        this.ships.push({locations:[],hits:["",""],collision:[],length:2,number:5});
        this.ships.push({locations:[],hits:["",""],collision:[],length:2,number:6});
        this.ships.push({locations:[],hits:[""],collision:[],length:1,number:7});
        this.ships.push({locations:[],hits:[""],collision:[],length:1,number:8});
        this.ships.push({locations:[],hits:[""],collision:[],length:1,number:9});
        this.ships.push({locations:[],hits:[""],collision:[],length:1,number:10});
     
        return this;
    },
    checkDouble: function (guess) {
            var index = this.locations.indexOf(guess);
            if (!~index){
                this.locations.push(guess);
                this.fire(guess);
                return true;
            }
        return false;
    },
    isSunk: function(ship) {
        for (var i = 0; i < ship.length; i++) {
            if (ship.hits[i] !== "hit") {
                return false;
            }
        }
        return true;
    },
    
    fire: function(guess) {
        if (!this.access) return;
        for (var i = 0; i < this.numShips; i++) {
            var ship = this.ships[i];
            var index = ship.locations.indexOf(guess);
            if (index >= 0){
                ship.hits[index] = "hit";
                inspector.hit(guess, this.type);
                this.hits++;
                inspector.scoreTable(this.hits, "hit", this.type);
                this.hited = true;
                this.guessed++;
                if (this.isSunk(ship)){
                    for (var x = 0; x < ship.collision.length; x++){
                        if (!ship.collision[x]) continue;
                        inspector.miss(ship.collision[x], this.type);
                            if (this.locations.indexOf(ship.collision[x]) < 0){
                                this.locations.push(ship.collision[x]);
                            }
                    }
                    inspector.kill(ship, this.type);
                    this.shipsSunk++;
                    inspector.scoreTable(this.shipsSunk, "kill", this.type);
                    console.log(this.shipsSunk);
                    if (this.shipsSunk == this.ships.length) {
                        if (this.type == "u"){
                            this.access = false;
                            bot.access = false;
                        }else{
                            this.access = false;
                            users.access = false;
                        }
                        Game.win(this.type);
                    }
                }
                return true;
            }
        }
        inspector.miss(guess, this.type);
        this.misses++;
        inspector.scoreTable(this.misses, "miss", this.type);
        this.hited = false;
        this.guessed++;
        
        return false;
    },
    
    generateShipLocations: function() {
        for (var t = 0; t < this.numShips; t++){
            var generatingShip = this.ships[t].length;
        
            var locations;
            var iteration = 0;
                do {
                    iteration++;
                    locations = this.generateShip(generatingShip);
                } while (this.collision(locations[0]));

                this.ships[t].collision = locations[1];
                this.ships[t].locations = locations[0];

//                console.log("Generated is " + iteration);
//                console.log("locations = [" + this.ships[t].locations +
//                            "]\n Row = " + this.ships[t].locations[0].charAt(0) +
//                            "\n Col = " + this.ships[t].locations[0].charAt(1) +
//                            "\nCollisons = [" + this.ships[t].collision + 
//                            "]\n------------------------------");

            
        }
        
    },
    generateShip: function(shipLength) {
        var direction = Math.floor(Math.random() * 2);
        var row, col;
        // 1 - горизонталь, 0 - вертикаль
        if (direction === 1) {
            row = Math.floor(Math.random() * this.boardSize);
            col = Math.floor(Math.random() * (this.boardSize - shipLength));
        } else {
            row = Math.floor(Math.random() * (this.boardSize - shipLength));
            col = Math.floor(Math.random() * this.boardSize);
        }
        
        var newShipLocations = [];
        
        for (var i = 0; i < shipLength; i++) {
            if (direction === 1) {
                newShipLocations.push(row + "" + (col + i));
            } else {
                newShipLocations.push((row + i) + "" + col);
            }
        }
        
        
        var collisions = [];
        
        var createdCollisons = this.genereteObstacles(newShipLocations, direction);
        
        var completeShip = [newShipLocations, createdCollisons];
        
        return completeShip;
    },
    genereteObstacles: function (createdShip, direction) {
        var row = +createdShip[0].charAt(0); 
        var col = +createdShip[0].charAt(1);

        var collisions1 = [];
		var collisions2 = [];
        //Горизонтальное положение
        if (direction){
			collisions1.push(row+""+(col-1));
            collisions1.push(row+""+(col+createdShip.length>=10?-1:col+createdShip.length));
            collisions1.push((row-1)+""+(col-1));
            collisions1.push((row+1>=10?-1:row+1)+""+(col-1));
            collisions1.push((row-1)+""+(col+createdShip.length>=10?-1:col+createdShip.length));
            collisions1.push((row+1>=10?-1:row+1)+""+(col+createdShip.length>=10?-1:col+createdShip.length));
            for (var c = 0; c < createdShip.length; c++) {
                collisions1.push((row+1>=10?-1:row+1)+""+(col+c>=10?-1:col+c));
                collisions1.push((row-1)+""+(col+c>=10?-1:col+c));
            }
        //Вертикальное положение
        }else{
            collisions1.push((row-1)+""+col);
            collisions1.push((row+createdShip.length>=10?-1:row+createdShip.length)+""+col);
            collisions1.push((row-1)+""+(col-1));
            collisions1.push((row-1)+""+(col+1>=10?-1:col+1));
            collisions1.push((row+createdShip.length>=10?-1:row+createdShip.length)+""+(col-1));
            collisions1.push((row+createdShip.length>=10?-1:row+createdShip.length)+""+(col+1>=10?-1:col+1));
             for (var c = 0; c < createdShip.length; c++) {
                 collisions1.push((row+c>=10?-1:row+c)+""+(col+1>=10?-1:col+1));
                 collisions1.push((row+c>=10?-1:row+c)+""+(col-1));
             }
        }
		
		for (var i = 0; i < collisions1.length; i++) {
                    if (  collisions1[i].search(/\-\d\-\d/)>-1
                        ||collisions1[i].search(/\-\d\d/)>-1
                        ||collisions1[i].search(/\d\-\d/)>-1){
                        collisions1[i] = collisions1[i].replace(/\-\d\-\d/, "");
                        collisions1[i] = collisions1[i].replace(/\-\d\d/, "");
                        collisions1[i] = collisions1[i].replace(/\d\-\d/, "");
                    }
            }
		
		for (var i = 0; i < collisions1.length; i++) {
			if (!collisions1[i]) continue;
			collisions2.push(collisions1[i]);
		}
		
		
            return collisions2;

    },
    collision: function(locations) {
        for (var i = 0; i < this.numShips; i++) {
            
            var ship = this.ships[i];
            
            for (var j = 0; j < locations.length; j++) {
                
                if (ship.collision.indexOf(locations[j]) >= 0 || ship.locations.indexOf(locations[j])  >= 0) {
                    return true;               
                }
            }
        }
        return false;
    },
    
    shipPos: function () {
        if (this.type === "b"){
            for (var i = 0; i < this.numShips; i++){
                var ship = this.ships[i];
                for (var j = 0; j < ship.length; j++){
                    $("#mGameBox tr>#" + ship.locations[j]).css("background", "lime");
                }
            }
        }else{
            for (var i = 0; i < this.numShips; i++) {
                var ship = this.ships[i];
                for (var j = 0; j < ship.length; j++){
                    $("#eGameBox tr>#" + ship.locations[j]).css("background", "lime");
                }
            }
        }
        
    },
    shipCol: function () {
        if (this.type == "u"){
            for (var i = 0; i < this.numShips; i++){
                var ship = this.ships[i];
                for (var j = 0; j < ship.collision.length; j++){
                    $("#eGameBox tr>#" + ship.collision[j]).addClass("miss");
                }
            }
        }else {
            for (var i = 0; i < this.numShips; i++){
                var ship = this.ships[i];
                for (var j = 0; j < ship.collision.length; j++){
                    $("#mGameBox tr>#" + ship.collision[j]).addClass("miss");
                }
            }
        }
      
        
    },
    
    restartGame: function () {
//        $("td").removeAttr("class");
        this.endGame();
        this.generateShipLocations();
    },
    endGame: function () {
        for (var j = 0; j < this.ships.length; j++){
            do {
                this.ships[j].locations.pop();
            }while(this.ships[j].locations.length);
            
            do {
                this.ships[j].collision.pop();
            }while(this.ships[j].collision.length);
            
            do {
                this.locations.pop();
            }while(this.locations.length);
                

            for (var i = 0; i < this.ships[j].hits.length; i++){
                this.ships[j].hits[i] = "";
            }
        }
        this.countHit = 0;
        this.nowShipHited = [];
        this.locations.push("");
        this.successHit = "";
        this.hits = 0;
        this.misses = 0;
        this.shipsSunk = 0;
    }
};

var inspector = {
    scoreTable: function (exp, type, typePlayer) {
        if (typePlayer === "u") {
            switch (type) {
                case "hit": $(".rightSidebar .hits>span").html(exp);
                    break;
                case "miss": $(".rightSidebar .misses>span").html(exp);
                    break;
                case "kill": $(".rightSidebar .kills>span").html(exp);
                    break;
            }
        }else{
            switch (type) {
                case "hit": $(".leftSidebar .hits>span").html(exp);
                    break;
                case "miss": $(".leftSidebar .misses>span").html(exp);
                    break;
                case "kill": $(".leftSidebar .kills>span").html(exp);
                    break;
            }
        }
    },
    hit: function (location, type) {
        if (type === "u")
            $("#eGameBox tr>#" + location).addClass("hit");
        else $("#mGameBox tr>#" + location).addClass("hit");
    },
    miss: function (location, type) {
        if (type === "u")
            $("#eGameBox tr>#" + location).addClass("miss");
        else $("#mGameBox tr>#" + location).addClass("miss");
    },
    kill: function (shipL, type){
        if (type === "u") for (var i = 0; i < shipL.length; i++)
            $("#eGameBox tr>#" + shipL.locations[i]).addClass("kill");
        else for (var i = 0; i < shipL.length; i++)
             $("#mGameBox tr>#" + shipL.locations[i]).addClass("kill");
    },
    live: function (shipL, type){
        if (type === "u") for (var i = 0; i < shipL.length; i++)
            $("#eGameBox tr>#" + shipL.locations[i]).addClass("live");
        else for (var i = 0; i < shipL.length; i++)
             $("#mGameBox tr>#" + shipL.locations[i]).addClass("live");
    }
};

var users = Object.create(model).constructor(10, 2, "u", "b");
var bot = Object.create(model).constructor(10, 2, "b", "u");

bot.checkDouble = function (guess) {
            var index = this.locations.indexOf(guess);
            if (index < 0){
                this.locations.push(guess);
                return true;
            }
        return false;
    },
bot.generateGuess = function () {
    if (this.locations.length > 100) return;
    var x,y;
    do{
        x = Math.floor(Math.random() * 10);
        y = Math.floor(Math.random() * 10);
    }while(!this.checkDouble(x + "" + y));
    return x+""+y;
}

// Хранит атакуемый корабль
bot.nowShipHited = [];
// Хранит первое попадание по корбалю и ничего больше!
bot.successHit = "";
// Хранит число попадании
bot.countHit = 0;
bot.smartFire = function () {
    if (!this.access) return;
    
    var getRandom, hitQ;
    if (this.nowShipHited.length && this.nowShipHited[0].length > 1){
        
        do{
            getRandom = this.generateGuess();     
        } while (!this.nowShipHited[0].includes(getRandom) 
               && !this.successHit.includes(getRandom));
                
        hitQ = this.fire(getRandom);
        this.countHit++;
        if (this.nowShipHited[0].length == this.countHit){
            this.nowShipHited.pop();
            this.successHit = [];
            this.countHit = 0;
        }
        
        
    }else{
        this.nowShipHited.pop();
        getRandom = this.generateGuess();
        hitQ = this.fire(getRandom);
        //Если попал по кораблю записать в массив попадание чтобы не попасть снова!
        if (hitQ){
            for (var i = 0; i < this.ships.length; i++){
                var index = this.ships[i].locations.indexOf(getRandom);
                if (index >= 0){
                    if (this.ships[i].locations.length > 1){
                        bot.nowShipHited.push(this.ships[i].locations);
                        this.successHit = getRandom;
                        this.countHit++;
                    }
                }
            }
        }
    }
    this.locations = getNewArr();
}

function getNewArr () {
    var newArr = [];
    $("#mGameBox tr>td").each(function () {
        if ($(this).hasClass("kill") || $(this).hasClass("miss") || $(this).hasClass("hit"))
            if (!newArr.includes(this.id))
                newArr.push(this.id);
    });
    return newArr;
}
